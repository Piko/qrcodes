module_size = 10

def chunkify(lst, n):
    return [lst[i:i+n] for i in range(0, len(lst), n)]

class Line:
    def __init__(self, x1, y1, x2, y2, stroke, width=module_size/4):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.stroke = stroke
        self.width = width

def evaluate_pattern(i, j, patterntype):
    match patterntype:
        case 0:
            return (i*j)%2 + (i*j)%3 == 0
        case 1:
            return (i//2 + j//3) % 2 == 0
        case 2:
            return ((i*j) % 3 + i + j) % 2 == 0
        case 3:
            return ((i*j) % 3 + i*j) % 2 == 0
        case 4:
            return i % 2 == 0
        case 5:
            return (i+j) % 2 == 0
        case 6:
            return (i+j) % 3 == 0
        case 7:
            return j % 3 == 0


class QRCode:
    colorschemes = {
        "clown": {
            "byte_border": "rgb(50,150,255)", 
            "length_border": "yellow", 
            "mode_border": "red",
            "mask_dark": "black",
            "mask_bright": "white", 
            "finder_patterns_bright": "white",
            "finder_patterns_dark": "orange",
            "alignment_pattern_bright": "white",
            "alignment_pattern_dark": "green",
            "calibration_module": "red",
            "timing_pattern_dark": "red",
            "timing_pattern_bright": "pink",
            "header": "red",
            "zigzag_line": "blue",
        },
        "nun": {
            "byte_border": "rgb(70,88,224)", 
            "length_border": "yellow", 
            "mode_border": "red",
            "mask_dark": "black",
            "mask_bright": "white", 
            "finder_patterns_bright": "white",
            "finder_patterns_dark": "lightgray",
            "alignment_pattern_bright": "white",
            "alignment_pattern_dark": "lightgray",
            "calibration_module": "lightgray",
            "timing_pattern_dark": "lightgray",
            "timing_pattern_bright": "white",
            "header": "rgb(255, 183, 197)",
            "zigzag_line": "orange",
        }
    }

    def __init__(self, version, mask_type, colorscheme="nun"):
        if version > 7:
            raise ValueError("Versions higher than 7 are not supported.")
        self.width = 17 + 4 * version
        self.version = version
        self.mask_type = mask_type
        self.colorscheme = QRCode.colorschemes[colorscheme]
        self.make_empty()
        self.add_timing_patterns()
        self.add_finder_patterns()
        self.add_alignment_patterns()
        self.add_header()
        self.add_calibration_module()

        self.draw_zigzag()
        self.draw_byte_outlines()

        self.fill_with_mask(mask_type)

    def make_empty(self):
        self.grid = []
        self.lines = []

        for i in range(self.width):
            self.grid.append([])
            for j in range(self.width):
                self.grid[i].append(None)


    def fill_with_mask(self, patterntype):
        self.patterntype = patterntype  
        for y in range(self.width):
            for x in range(self.width):
                if self.grid[y][x] is None:
                    if evaluate_pattern(x, y, patterntype):
                        self.grid[y][x] = self.colorscheme["mask_dark"]
                    else:
                        self.grid[y][x] = self.colorscheme["mask_bright"]

    def add_finder_pattern(self, ox, oy):
        light_color = self.colorscheme["finder_patterns_bright"]
        dark_color = self.colorscheme["finder_patterns_dark"]
        self.add_square(ox, oy, 9, light_color)
        self.add_square(ox+1, oy+1, 7, dark_color)
        self.add_square(ox+2, oy+2, 5, light_color)
        self.add_square(ox+3, oy+3, 3, dark_color)

    def add_square(self, ox, oy, size, color):
        for y in range(size):
            for x in range(size):
                # Calculate effective coordinates.
                ex = x + ox
                ey = y + oy
                if 0 <= ex < self.width and 0 <= ey < self.width:
                    self.grid[ey][ex] = color

    def add_finder_patterns(self):
        self.add_finder_pattern(-1, -1)
        self.add_finder_pattern(self.width-8, -1)
        self.add_finder_pattern(-1, self.width-8)

    def add_alignment_patterns(self):
        if 1 < self.version < 7:
            for y in range(5):
                for x in range(5):
                    if 0 < y < 4 and 0 < x < 4 and not (x==2 and y==2):
                        color = self.colorscheme["alignment_pattern_bright"]
                    else: 
                        color = self.colorscheme["alignment_pattern_dark"]
                    self.grid[y+self.width-9][x+self.width-9] = color

    def add_header(self):
        y = 8
        for x in range(self.width):
            if not (8 < x < self.width-8):
                color = self.colorscheme["header"]
                self.grid[y][x] = color
                self.grid[x][y] = color

    def add_calibration_module(self):
       self.grid[8][self.width-8] = self.colorscheme["calibration_module"] 

    def add_timing_patterns(self):
        y = 6
        for x in range(self.width):
            color = self.colorscheme["timing_pattern_dark"] if x%2 == 0 else self.colorscheme["timing_pattern_bright"]
            self.grid[y][x] = color
            self.grid[x][y] = color

    def mark_outline(self, coordinates, color):
        for coordinate in coordinates:
            left_neighbor = (coordinate[0]-1, coordinate[1])  # (x, y)
            right_neighbor = (coordinate[0]+1, coordinate[1])
            upper_neighbor = (coordinate[0], coordinate[1]-1)
            lower_neighbor = (coordinate[0], coordinate[1]+1)
            if upper_neighbor not in coordinates:
                self.lines.append(Line(coordinate[0], coordinate[1], coordinate[0]+1, coordinate[1], color))
            if lower_neighbor not in coordinates:
                self.lines.append(Line(coordinate[0]+1, coordinate[1]+1, coordinate[0], coordinate[1]+1, color))
            if left_neighbor not in coordinates:
                self.lines.append(Line(coordinate[0], coordinate[1], coordinate[0], coordinate[1]+1, color))
            if right_neighbor not in coordinates:
                self.lines.append(Line(coordinate[0]+1, coordinate[1]+1, coordinate[0]+1, coordinate[1], color))

    def column_positions_for_zigzag(self):
        for x in range(self.width-1, 6, -2):
            yield x
        for x in range(5, 0, -2):
            yield x

    def y_coordinates_for_zigzag(self, go_up):
        if go_up:
            for y in range(self.width-1, -1, -1):
                yield y
        else:
            for y in range(0, self.width):
                yield y

    def generate_zigzag(self):
        go_up = True
        for x in self.column_positions_for_zigzag():
            for y in self.y_coordinates_for_zigzag(go_up):
                for x_offset in range(0, -2, -1):
                    module_x = x + x_offset
                    module_y = y
                    if self.grid[module_x][module_y] is None:
                        yield (module_x, module_y)
            go_up = not go_up

    def draw_zigzag(self):
        previous = None
        for coordinate in self.generate_zigzag():
            if previous is not None:
                self.lines.append(Line(previous[0] + 0.5, previous[1] + 0.5, coordinate[0] + 0.5, coordinate[1] + 0.5, self.colorscheme["zigzag_line"], module_size/8))
            previous = coordinate

    def draw_byte_outlines(self):
        coordinates = list(self.generate_zigzag())

        content = chunkify(coordinates[4+8:], 8)
        for byte in content:
            self.mark_outline(byte, self.colorscheme["byte_border"])

        length = coordinates[4:4+8]
        self.mark_outline(length, self.colorscheme["length_border"])

        typ = coordinates[:4]
        self.mark_outline(typ, self.colorscheme["mode_border"])

    def export_svg(self, filename="qr"):
        svg = f'<svg width="{self.width*module_size}" height="{self.width*module_size}">'
        for i in range(self.width):
            for j in range(self.width):
                color = self.grid[j][i] or "magenta"
                svg += f'<rect x="{j*module_size}" y="{i*module_size}" width="{module_size}" height="{module_size}" style="fill:{color}"/>'
        for line in self.lines:
            svg += f'<line x1="{line.x1*module_size}" y1="{line.y1*module_size}" x2="{line.x2*module_size}" y2="{line.y2*module_size}" style="stroke:{line.stroke}; stroke-width: {line.width}px" stroke-linecap="round"/>'

        svg += '</svg>'
        with open(filename + ".svg", "w") as f:
            f.write(svg)

# generate_all_patterns()
for mask_type in range(8):
    q = QRCode(2, mask_type)
    q.export_svg(f"qr{mask_type}")
